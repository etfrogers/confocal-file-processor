# -*- coding: utf-8 -*-
"""
    Created on Mon Apr 24 11:22:03 2017
    
    Confocal file processor - strips data from tiff files and build and image 
    manifest (and optionally images in different formats) for input to Zegami
    
    Copyright (C) 2017  Edward Rogers

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    @author: Ed Rogers
"""

import queue
import threading

exitFlag = 0

queueLock = threading.Lock()
workQueue = queue.Queue(0)


class ConversionWorker(threading.Thread):
    def __init__(self, thread_id, name, q, eight_bit_pngs, fmt):
        threading.Thread.__init__(self)
        self.threadID = thread_id
        self.name = name
        self.q = q
        self.eightBitPNGs = eight_bit_pngs
        self.fmt = fmt

    def run(self):
        print("Starting worker " + self.name)
        process_data(self.name, self.q, self.eightBitPNGs, self.fmt)
        print("Exiting worker " + self.name)


def process_data(thread_name, q, eight_bit_pngs, fmt):
    while not exitFlag:
        queueLock.acquire()
        if not workQueue.empty():
            file = q.get()
            queueLock.release()
            file.tiff_to_zegami(eight_bit_pngs, fmt)
        else:
            queueLock.release()
        


