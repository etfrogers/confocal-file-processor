#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Created on Sat Apr  8 07:11:59 2017
    Confocal file processor - strips data from tiff files and build and image 
    manifest (and optionally images in different formats) for input to Zegami
    
    Copyright (C) 2017  Edward Rogers

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    @author: Ed Rogers
"""
import os
import math
import glob
import exifread
import xml.etree.ElementTree as ElementTree
import datetime
from PIL import Image
import numpy


def str2bool(value):
    """
       Converts 'something' to boolean. Raises exception for invalid formats
           Possible True  values: 1, True, "1", "TRue", "yes", "y", "t"
           Possible False values: 0, False, None, [], {}, "", "0", "faLse", "no", "n", "f", 0.0, ...
    """
    if str(value).lower() in ("yes", "y", "true",  "t", "1"):
        return True
    if str(value).lower() in ("no",  "n", "false", "f", "0", "0.0", "", "none", "[]", "{}"):
        return False
    raise Exception('Invalid value for boolean conversion: ' + str(value))


class Session:
    def __init__(self, name=''):
        self.name = name
        
        
class Experiment:
    
    experimentList = {}
    
    @staticmethod
    def get_experiment(session, name, full_dir):
        temp_key = session.name + ':' + name
        if temp_key in Experiment.experimentList:
            exp = Experiment.experimentList[temp_key]
        else:
            exp = Experiment(name, session, full_dir)
            Experiment.experimentList[temp_key] = exp
        return exp
           
    def __init__(self, name='', session=None, full_dir=''):
        self.name = name
        self.dataIsValid = False
        self.fullDir = full_dir
        self.session = session
        self.load_data()
        
    @property
    def key(self):
        return self.session.name + ':' + self.name
        
    @property
    def data(self):
        data_dict = {'Polscope Use Presets': self.polPresets,
                     'Date': self.date.strftime('%d/%m/%Y'),
                     'Time': self.date.strftime('%H:%M:%S'),
                     'Pixel size X': self.dx, 'Pixel size Y': self.dy,
                     'Pixel size Z': self.dz, 'Time Step': self.dt,
                     'Size X': self.Nx, 'Size Y': self.Ny, 'Size Z': self.Nz,
                     'Size T': self.Nt, 'Number of Channels': self.Nc}
        return data_dict

    # noinspection PyAttributeOutsideInit
    def load_data(self):
        # Doesn't matter if it is first file as long as it is a raw (unprocessed)
        # file. Extracted data should be same for all files.

        # noinspection PyBroadException
        try:
            try:
                first_file = glob.glob(self.fullDir+'/Chan*_0001_0001_0001_0001_0001.tif')[0]
            except IndexError:
                first_file = glob.glob(self.fullDir+'/Chan*_0001_0001_0001_0001.tif')[0]
        except:
            print('Skipping experiment data:' + self.fullDir)
            self.dataIsValid = False
            return None
    
        with open(first_file, 'rb') as imgFile:
            tags = exifread.process_file(imgFile)
    
        ome_xml_str = tags['Image ImageDescription'].values
        ome_xml = ElementTree.fromstring(ome_xml_str)
        # TODO Maybe get individual times for each image from ome_xml?
        self.dx = ome_xml[0][0].attrib['PhysicalSizeX']
        self.dy = ome_xml[0][0].attrib['PhysicalSizeY']
        self.dz = ome_xml[0][0].attrib['PhysicalSizeZ']
        self.dt = ome_xml[0][0].attrib['TimeIncrement']
        self.Nx = ome_xml[0][0].attrib['SizeX']
        self.Ny = ome_xml[0][0].attrib['SizeY']
        self.Nz = ome_xml[0][0].attrib['SizeZ']
        self.Nc = ome_xml[0][0].attrib['SizeC']
        self.Nt = ome_xml[0][0].attrib['SizeT']
        date_str = ome_xml[0].attrib['AcquiredDate']

        ome_date = datetime.datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S.%f-00:00')

        thor_image_xml = ElementTree.parse(os.path.join(self.fullDir, 'Experiment.xml'))
        thor_date_str = thor_image_xml.find('Date').attrib['date']
        thor_date = datetime.datetime.strptime(thor_date_str, '%m/%d/%Y').date()
        assert(thor_date == ome_date.date())

        self.date = ome_date
        try:    
            self.polPresets = str2bool(thor_image_xml.find('Polscope').attrib['PolscopeUsePresets'])
        except (KeyError, AttributeError):
            self.polPresets = False
            
        if self.polPresets:
            self.angles = ['', '', '', '']
        else:
            try:
                self.angles = [val.text for val in thor_image_xml.find('Polscope').findall('Angle')]
            except AttributeError:
                self.angles = ['', '', '', '']
        self.dataIsValid = True


class DataFile:
    def __init__(self, full_path=''):
        self.path = full_path
        self.dataIsValid = False
        self.load_tiff_data()
    
    @property
    def filename_no_ext(self):
        filename_no_ext, _ = os.path.splitext(self.fname)
        return filename_no_ext
    
    @property
    def data(self):
        if not self.dataIsValid:
            raise AttributeError
        data_dict = {'Session': self.session.name,
                     'Experiment': self.experiment.name,
                     'File': self.filename_no_ext,
                     'Channel': self.channel, 'X step': self.x_ind,
                     'Y step': self.y_ind, 'T step': self.t_ind,
                     'Z step': self.z_ind, 'IsPolscope': self.isPolscope,
                     'Polscope Image Type': self.polscopeType,
                     'Is Polscope Raw Image': self.polscopeRaw, 'Angle': self.angle,
                     'IsMidpoint': self.isMidpoint}
                    
        if self.experiment.dataIsValid:
            data_dict.update(self.experiment.data)
        else:
            print('Mising experiment data: ' + self.experiment.name)
        return data_dict

    # noinspection PyAttributeOutsideInit
    def load_tiff_data(self):
        parts = self.path.split(os.sep)
        
        session_name = parts[-3]
        self.session = Session(session_name)
        experiment_name = parts[-2]
        self.fname = parts[-1]
        
        (expDir, _) = os.path.split(self.path)
        fparts = self.filename_no_ext.split('_')
    
        if(fparts[0][0:4] != 'Chan') or (fparts[1] == 'Preview'):
            self.dataIsValid = False
            return None
        
        self.channel = fparts[0][4]
        self.x_ind = int(fparts[1])
        self.y_ind = int(fparts[2])
        self.t_ind = int(fparts[3])
        self.z_ind = int(fparts[4])
        self.isPolscope = len(fparts) > 5
    
        if self.isPolscope:
            try:
                self.polscopeType = int(fparts[5])
                self.polscopeRaw = True
            except ValueError:
                self.polscopeType = fparts[5]
                if len(fparts) > 6:
                    assert(fparts[6] == 'CMProcessed')
                    self.polscopeType += ' Color'
                self.polscopeRaw = False
        else:
            self.polscopeType = None
            self.polscopeRaw = False
        
        # expData is the data that is constant over an experiment, and does not 
        # vary between images
        self.experiment = Experiment.get_experiment(self.session, experiment_name, expDir)
        
        if self.experiment.dataIsValid:

            angles = self.experiment.angles
            if self.polscopeRaw:
                self.angle = angles[self.polscopeType-1]
            else:
                self.angle = ''
        
            self.isMidpoint = ((self.t_ind == math.ceil(float(self.experiment.data['Size T'])/2)) and 
                               (self.z_ind == math.ceil(float(self.experiment.data['Size Z'])/2)))
        else:
            self.angle = ''
            self.isMidpoint = False
            
        self.dataIsValid = True
    
    def tiff_to_zegami(self, eight_bit, fmt):
        assert(fmt == 'jpg' or fmt == 'png')
        assert(self.fname.endswith('.tif'))
        file_dir, _ = os.path.split(self.path)

        png_dir = os.path.join(file_dir, fmt)
        try:
            os.mkdir(png_dir)
        except FileExistsError:
            pass
        
        try:
            im = Image.open(self.path)
            if im.mode == 'I;16':
                im = im.convert(mode='I')
                if eight_bit:
                    (mi, ma) = get_extrema(im)
                    table = numpy.concatenate((numpy.zeros([mi]), 
                                               numpy.linspace(0, 255, ma-mi),
                                               numpy.zeros([65536 - ma])))
                    im2 = im.point(table, 'L')
                    im = im2

            im.save(os.path.join(png_dir, self.filename_no_ext + '.' + fmt))
        except OSError:
            print('Skipping %s file for: %s' % (fmt.upper(), self.path))


def get_extrema(im):
    """
    im.getextrema() should do this, but doesn't seem to be working
    I have re-implemented for now, but may fix later
    """
    a = numpy.asarray(im)
    ex = (numpy.amin(a), numpy.amax(a))
    return ex        
