#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    Created on Mon Apr  3 19:47:24 2017
    
    Confocal file processor - strips data from tiff files and build and image 
    manifest (and optionally images in different formats) for input to Zegami
    
    Copyright (C) 2017  Edward Rogers

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    @author: Ed Rogers
"""
import os
import glob
import csv
import shutil
import math
from file_classes import DataFile, Experiment, Session
import conversion_worker
import queue
import time
import multiprocessing
import argparse


def find_tiff_files(path):
    return find_files_recursive(path, '.tif')


def find_avi_files(path):
    return find_files_recursive(path, '.avi')


def find_files_recursive(path, ext):
    files = [file for file in glob.iglob(path + '/**/*' + ext, recursive=True)]
    return files        


def process_tiff_files(path):
    file_list = find_tiff_files(path)
    # remove thumbnail files. Currently conservative and assume thumbnails 
    # folder is subdir of path
    file_list = [file for file in file_list if not file.startswith(os.path.join(path, 'thumbnails'))]
                
    if file_list is not None:
        data_files = [DataFile(filename) for filename in file_list]
        
        # remove blank entries from both data_files
        data_files = [file for file in data_files if file.dataIsValid]
        
        # TODO use enumerate
        # print(dataLines)
        list_of_dicts = [file.data for file in data_files]
        uid = 0
        for line in list_of_dicts:
            line.update({'ID': uid})
            uid += 1
            
        csv_name = os.path.join(path, 'image_manifest.csv')
        write_dicts_to_file(csv_name, list_of_dicts)
        return data_files
    else:
        print('No files found')
        return None, file_list
    

def write_dicts_to_file(filename, list_of_dicts):
    with open(filename, 'w', newline='') as csvfile:
        fieldnames = ['ID', 'Session', 'Experiment', 'Date', 'Time', 'File', 'Channel', 'X step', 'Y step', 'T step',
                      'Z step', 'IsPolscope', 'Is Polscope Raw Image', 'Polscope Image Type', 'Polscope Use Presets',
                      'Angle', 'IsMidpoint', 'Pixel size X', 'Pixel size Y', 'Pixel size Z', 'Time Step',
                      'Size X', 'Size Y', 'Size Z', 'Size T', 'Number of Channels']

        assert(len(fieldnames) == len(list_of_dicts[0].keys()))
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        writer.writerows(list_of_dicts)


def make_thumbnails(path, file_list):
    keys = [file.experiment.key for file in file_list]
    keys = set(keys)
    # make thumbnail dir
    pre_session_path_ind = file_list[0].path.find(file_list[0].session.name)
    assert (pre_session_path_ind >= 0)
    pre_session_path = file_list[0].path[:pre_session_path_ind]
    thumbnail_dir = os.path.join(path, 'thumbnails')
    if not os.path.exists(thumbnail_dir):
        os.mkdir(thumbnail_dir)
    
    for key in keys:
        session_name, exp_name = key.split(':')
        session = Session(session_name)
        full_exp_dir = os.path.join(pre_session_path, session_name, exp_name)
        exp = Experiment.get_experiment(session, exp_name, full_exp_dir)
        if exp.dataIsValid:
            mid_t = math.ceil(float(exp.Nt)/2)
            mid_z = math.ceil(float(exp.Nz)/2)
            mid_trans_file = os.path.join(full_exp_dir,
                                          'ChanA_0001_0001_' + "%04d_%04d" % (mid_t, mid_z) + '_MeanTrans.tif')
            color_file = os.path.join(full_exp_dir,
                                      'ChanA_0001_0001_' + "%04d_%04d" % (mid_t, mid_z)
                                      + '_Diattenuation_CMProcessed.tif')
            mean_trans_thumb = session.name+'_'+exp.name+'_MeanTrans.tif'
            color_thumb = session.name+'_'+exp.name+'_Diattenuation_CMProcessed.tif'
            try:
                shutil.copy2(mid_trans_file, os.path.join(thumbnail_dir, mean_trans_thumb))
                shutil.copy2(color_file, os.path.join(thumbnail_dir, color_thumb))
            except FileNotFoundError:
                print('Skipping missing thumbnail: ' + full_exp_dir)
        else:
            print('Skipping thumbnail for missing experiment data: ' + full_exp_dir)
    print(keys)


def create_zegami_files(file_list, eight_bit, img_format):
    n_workers = multiprocessing.cpu_count()-1
    thread_list = [str(i) for i in range(0, n_workers)]
    # Fill the queue
    conversion_worker.workQueue = queue.Queue(len(file_list))
    conversion_worker.queueLock.acquire()
    for file in file_list:
        conversion_worker.workQueue.put(file)
    conversion_worker.queueLock.release()

    threads = []
    thread_id = 1
    # Create new threads
    for tName in thread_list:
        thread = conversion_worker.ConversionWorker(thread_id, tName, conversion_worker.workQueue,
                                                    eight_bit, img_format)
        thread.start()
        threads.append(thread)
        thread_id += 1
    
    # Wait for queue to empty
    while not conversion_worker.workQueue.empty():
        pass

    # Notify threads it's time to exit
    conversion_worker.exitFlag = 1
    
    # Wait for all threads to complete
    for t in threads:
        t.join()
    print("Exiting Main Thread")


def main():
    # TODO search AVIs?
    print("File processor")
    parser = argparse.ArgumentParser(description='Process directory of files to make them ready for Zegami')
    parser.add_argument('path',
                        help='The directory to process')
    parser.add_argument('-16', '--16bit-pngs', dest='sixteen_bit_pngs', action='store_const',
                        const=True, default=False,
                        help='Save as 16 bit PNGs (default: False)')
    parser.add_argument('--no-thumbnails', action='store_const',
                        const=True, default=False,
                        help='Skip making thumbnails (default: False)')
    parser.add_argument('--no-pngs', action='store_const',
                        const=True, default=False,
                        help='Skip making PNG files (default: False)')
    parser.add_argument('-f', '--format', choices=['png', 'jpg'], default='png',
                        help='Format of files to save. Only PNG of JPG are supported at the moment')

    args = parser.parse_args()
    print('Processing files from: ' + args.path)

    eight_bit_pngs = not args.sixteen_bit_pngs
    print("Building manifest")
    file_list = process_tiff_files(args.path)

    if not args.no_thumbnails:
        print('Creating thumbnails')
        make_thumbnails(args.path, file_list)

    if not args.no_pngs:
        print('Making PNG copies')
        t = time.time()
        create_zegami_files(file_list, eight_bit_pngs, args.format)
        elapsed = time.time() - t
        print('Elapsed: %s' % elapsed)


if __name__ == '__main__':
    main()
